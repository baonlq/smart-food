import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class Menu {

    foods: any = [
        {
            type: 'header',
            Name: 'Bữa sáng',
            slug: 'breakfast'
        },
        {
            id: 1,
            Name: 'Bún riêu cua',
            Recipe: '100g bún, 30g thịt cua, 50g đậu phụ rán, 100g cà chua, xoài xanh, gia vị, rau sống',
            Energy: 268,
            image: 'assets/images/bun-rieu.jpg',
            selected: false
        },
        {
            id: 2,
            Name: '1 ly nước chanh',
            Recipe: '250ml nước lọc+ ½ quả chanh tươi',
            Energy: 8,
            image: 'assets/images/nuoc-chanh.jpg',
            selected: false
        },
        {
            id: 3,
            Name: '1 ly sữa đậu nành có đường',
            Recipe: '250ml+ 3 thìa cafe đường',
            Energy: 70,
            image: 'assets/images/sua-dau-nanh.jpg',
            selected: false
        },
        {
            id: 4,
            Name: 'Nước lọc 1 ly',
            Recipe: '300ml',
            Energy: '0',
            image: 'assets/images/nuoc-loc.jpg',
            selected: false
        },
        {
            type: 'header',
            Name: 'Bữa trưa',
            slug: 'lunch'
        },
        {
            id: 5,
            Name: 'Cơm 1 bát vơi',
            Recipe: '70g gạo tẻ',
            Energy: '250',
            image: 'assets/images/com-ngon.jpg',
            selected: false
        },
        {
            id: 6,
            Name: 'Trứng gà đúc thịt',
            Recipe: '1 hộp',
            Energy: '105.5',
            image: 'assets/images/trung-ga.png',
            selected: false
        },
        {
            id: 7,
            Name: 'Rau cải luộc',
            Recipe: '200g (2 bát con rau)',
            Energy: '70',
            image: 'assets/images/rau-cai.jpeg',
            selected: false
        },
        {
            id: 8,
            Name: 'Hoa lí xào bò',
            Recipe: 'Hoa lí 100g+bò 50g+ dầu ăn 5g+tỏi 5g',
            Energy: '166',
            image: 'assets/images/hoa-thien-ly-xao-thit-bo.jpg',
            selected: false
        },
        {
            id: 9,
            type: 'header',
            Name: 'Bữa tối',
            slug: 'dinner'
        },
        {
            id: 10,
            Name: 'Khoai sọ luộc',
            Recipe: '100 g khoai sọ đã luộc chín',
            Energy: '93',
            image: 'assets/images/khoai-so.jpg',
            selected: false
        },
        {
            id: 11,
            Name: 'Sườn lợn  bí đỏ',
            Recipe: 'Sườn lợn 100g+ bí đỏ 200g',
            Energy: '240',
            image: 'assets/images/suon-lon-bi-do.png',
            selected: false
        },
        {
            id: 12,
            Name: 'Bưởi',
            Recipe: '5 múi',
            Energy: '50',
            image: 'assets/images/buoi.jpg',
            selected: false
        },
        {
            id: 13,
            Name: 'Ngô luộc 1 bắp lớn',
            Recipe: '1 bắp ngô tươi, đã bỏ vỏ và lõi',
            Energy: '200',
            image: 'assets/images/ngo-luoc.jpg',
            selected: false
        },
    ];

    static myMenu: any = [];

    constructor(public http: Http) {
        console.log('Hello Menu Provider');
    }

    getList() {
        return this.foods;
    }

    getMyMenu() {
        return Menu.myMenu;
    }

    add(item) {
        for (var i = 0; i < this.foods.length; i++) {
            let food = this.foods[i];
            if (item.id == food.id) {
                food.selected = true;
                Menu.myMenu.push(food);
                console.log(Menu.myMenu);
            }
        }
    }

    remove(item) {
        for (var i = 0; i < Menu.myMenu.length; i++) {
            let food = Menu.myMenu[i];
            if (item.id == food.id) {
                if (food.selected) {
                    food.selected = false;
                }
                Menu.myMenu.splice(i, 1);
            }
        }
    }

}
