import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import * as Parse from 'parse';
Parse.initialize(
    'dEwLROXLqZnsWtflVoztCHJwT89wl6SXPD5zWjUx',
    'ndqs0ZOG9QHPrqb4EBbEHVt3wpDl3BMZtJedJXje',
    'ojTZQ8O6Yjg0pGZ2HXGlQp2ZejOwmRYCp8zCsODF'
);
Parse.serverURL = 'https://parseapi.back4app.com';

@Injectable()
export class ParseService {

    constructor(public http: Http) {
        console.log('Hello Parse Provider');
    }

    isAuthenticated() {
        var currentUser = Parse.User.current();
        console.log(currentUser);
        if (currentUser != null) {
            return true;
        } else {
            return false;
        }
    }

    signIn(credential, successCB: (user) => void, errorCB: (error) => void) {
        Parse.User.logIn(credential.username, credential.password, {
            success: function(user) {
                // Do stuff after successful login.
                successCB(user);
            },
            error: function(user, error) {
                // The login failed. Check error to see why.
                errorCB(error);
            }
        });
    }

    register(credential, success: (user) => void, errorCB?: (error) => void) {
        var user = new Parse.User();
        user.set("username", credential.username);
        user.set("password", credential.password);
        user.set("email", credential.email);
        user.set("authData", {});
        user.set("dateOfBirth", credential.dateOfBirth);

        user.signUp(null, {
            success: function(user) {
                success(user);
            },
            error: function(user, error) {
                //alert("Error: " + error.code + " " + error.message);
                console.error('Parse: SignUp function has an error: ', error);
                if (errorCB) {
                    errorCB(error);
                }
            }
        });
    };

    getCurrentUser() {
        return Parse.User.current();
    }

    saveBodyIndex(age:string, height:string, weight: string, successCB: (user) => void, errorCB?: (error) => void) {
        var user = Parse.User.current();
        user.set('age', parseInt(age));
        user.set('height', parseFloat(height));
        user.set('weight', parseFloat(weight));

        user.save(null, {
            success: function(user) {
                successCB(user);
            },
            error: function(user, error) {
                errorCB(error);
            }
        });
    }

    logout() {

        Parse.User.logOut().then(() => {
            var currentUser = Parse.User.current();  // this will now be null
        });
    }

}
