import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {Menu} from '../../providers/menu';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
    providers: [Menu]
})
export class HomePage {

    mealType: string = 'morning';
    menus: any = [];

    constructor(public navCtrl: NavController,
        public menu: Menu) {

        this.getMenus();
    }

    getMenus() {
        this.menus = this.menu.getList();
    }

    addToMenu(m) {
        this.menu.add(m);
    }

    removeFromMenu(m) {
        this.menu.remove(m);
    }

    ionViewDidLoad() {
        console.log('Hello HomePage Page');
    }

}
