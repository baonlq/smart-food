import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';


@Component({
    selector: 'page-my-menu',
    templateUrl: 'my-menu.html'
})
export class MyMenuPage {

    menus: any = [];
    constructor(public navCtrl: NavController) {
      this.menus = [
          {
              Name: '1 ly nước chanh',
              Recipe: '250ml nước lọc+ ½ quả chanh tươi',
              Energy: 8,
              image: '/assets/images/nuoc-chanh-2.jpg'
          },
          {
              Name: 'Bún thịt băm',
              Recipe: '100g bún',
              Energy: 179.5,
              image: '/assets/images/bun-thit-bam.jpg'
          },
          {
              Name: 'Nước lọc 1 ly',
              Recipe: '300ml',
              Energy: '0',
              image: '/assets/images/nuoc-loc.jpg'
          },
          {
              Name: 'Sữa chua Vinamilk',
              Recipe: '1 hộp',
              Energy: '105.5',
              image: '/assets/images/sua-chua-vinamilk.jpg'
          },
          {
              Name: 'Cơm trắng 1 bát vơi',
              Recipe: '70g gạo tẻ',
              Energy: '250',
              image: '/assets/images/com-tam.jpg'
          },
          {
              Name: 'Canh hến',
              Recipe: 'Thịt hến 150g',
              Energy: '122.5',
              image: '/assets/images/canh-hen.jpg'
          },
          {
              Name: 'Rau muống luộc',
              Recipe: '200g',
              Energy: '50',
              image: '/assets/images/rau-muong-luoc.jpg'
          },
      ]
    }

    ionViewDidLoad() {
        console.log('Hello MyMenuPage Page');
    }

}
