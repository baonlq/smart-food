import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { ParseService } from '../../providers/parse';

import {LoginPage} from '../login/login';
import {TabsPage} from '../tabs/tabs';
import { User } from '../../models/User';

@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
    providers: [ParseService]
})
export class RegisterPage {

    model: any;
    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public parse: ParseService,
        public loadingCtrl: LoadingController) {

        this.model = new User('', '', '', '', '');
    }

    ionViewDidLoad() {
        console.log('Hello RegisterPage Page');
    }

    register() {
        if (this.model.username == ""
            || this.model.password == ""
            || this.model.email == ""
            || this.model.date == "") {

            let alert = this.alertCtrl.create({
                title: 'OOPS!!',
                subTitle: 'Please retype incorrect input!',
                buttons: ['OK']
            });
            alert.present();
            return;
        }

        if (this.model.password !== this.model.passwordConfirmation) {
            let alert = this.alertCtrl.create({
                title: 'OOPS!!',
                subTitle: 'Password Mismatch',
                buttons: ['OK']
            });
            alert.present();
            return;
        }

        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
        });
        loader.present();
        this.parse.register(this.model, (user) => {
            console.log('Register successful: ', user);
            loader.dismiss();

            this.navCtrl.setRoot(TabsPage);
        }, (error) => {
            loader.dismiss();
        })
    }

    backLogin() {
        this.navCtrl.push(LoginPage);
    }

}
