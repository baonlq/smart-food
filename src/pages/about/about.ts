import { Component, OnInit } from '@angular/core';

import {Menu} from '../../providers/menu';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
    providers: [Menu]
})
export class AboutPage {

    foods: any = [];
    constructor(public navCtrl: NavController,
        public menu: Menu) {

    }

    getMyMenuList() {
        console.log(this.menu.getMyMenu());
        this.foods = this.menu.getMyMenu();
    }

    removeFromMenu(m) {
        this.menu.remove(m);
    }

    ionViewDidEnter() {
        this.getMyMenuList();
    }

}
