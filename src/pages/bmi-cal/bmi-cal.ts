import {Component, OnInit} from '@angular/core';
import {NavController, AlertController, PopoverController, NavParams} from 'ionic-angular';
import {LoginPage} from '../login/login';
import {ParseService} from '../../providers/parse';
import {TabsPage} from '../tabs/tabs';

@Component({
    selector: 'page-bmi-cal',
    templateUrl: 'bmi-cal.html',
    providers: [ParseService]
})
export class BMICalPage implements OnInit{

    bmi: any;
    rebmi: number = 0.0;
    isLogInProcess:boolean;
    currentUser:any;

    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                public parse: ParseService,
                private navParams: NavParams) {
        this.bmi = {Age: null, Height: null, Weight: null};

        console.log('nav params: ', this.navParams.get('login'));
        if (this.navParams.get('login')) {
            this.isLogInProcess = this.navParams.get('login');
        }
    }

    ngOnInit() {
        this.currentUser = this.parse.getCurrentUser();
        this.bmi = {Age: this.currentUser.get('age'), Height: this.currentUser.get('height'), Weight: this.currentUser.get('weight')};
        this.mathBMI_Click();

    }

    logout() {
        this.parse.logout();
        this.navCtrl.setRoot(LoginPage);
    }

    decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    mathBMI_Click() {
        if (this.bmi.Height > 0 && this.bmi.Weight > 0) {
            this.rebmi = this.bmi.Weight / (this.bmi.Height / 100 * this.bmi.Height / 100);
            this.rebmi = this.decimalAdjust('round', this.rebmi, -1);   // 55.6

            this.parse.saveBodyIndex(this.bmi.Age, this.bmi.Height, this.bmi.Weight, (user) => {
                if (this.isLogInProcess) {
                    this.navCtrl.push(TabsPage);
                }
            }, (error) => {
                let alert = this.alertCtrl.create({
                    title: 'Lỗi',
                    subTitle: 'Hệ thống lỗi!',
                    buttons: ['OK']
                });
                alert.present();
            })
        }
        else {
            let alert = this.alertCtrl.create({
                title: 'Lỗi',
                subTitle: 'Bạn chưa nhập đủ thông tin!',
                buttons: ['OK']
            });
            alert.present();
        }
    }

    ionViewDidLoad() {
        console.log('Hello HomePage Page');
    }

}
