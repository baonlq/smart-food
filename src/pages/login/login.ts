import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';

import {RegisterPage} from '../register/register';
import {TabsPage} from '../tabs/tabs';
import {ParseService} from '../../providers/parse';
import { User } from '../../models/User';
import { BMICalPage } from '../bmi-cal/bmi-cal';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    model: any;
    constructor(public navCtrl: NavController,
        public parse: ParseService,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController) {
        this.model = new User('', '', '', '', '');
    }

    ionViewDidLoad() {
        console.log('Hello LoginPage Page');
    }

    createAccount() {
        this.navCtrl.push(RegisterPage);
    }

    login() {
        if (this.model.password == "" || this.model.password == null || this.model.username == "" || this.model.username == null) {
            let alert = this.alertCtrl.create({
                title: 'OOPS!!',
                subTitle: 'Please input username and password!',
                buttons: ['OK']
            });
            alert.present();
            return;
        }
        else {
            let loading = this.loadingCtrl.create({
                content: "Please wait..."
            });
            loading.present();
            this.parse.signIn(this.model, (user) => {
                loading.dismiss();

                if (!user.get('age') || !user.get('height') || !user.get('weight')) {
                    this.navCtrl.push(BMICalPage, {login: true});
                } else
                    this.navCtrl.push(TabsPage);
            }, (error) => {
                loading.dismiss();
                let alert = this.alertCtrl.create({
                    title: 'OOPS!!',
                    subTitle: 'Wrong username or password',
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    }

}
