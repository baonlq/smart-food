import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';

@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html'
})
export class ContactPage {
    bmi: any;
    rebmi: number = 0.0;
    constructor(public navCtrl: NavController
                , public alertCtrl: AlertController) {
        this.bmi = { Age: null, Height: null, Weight: null };
    }

    decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    mathBMI_Click() {
        if (this.bmi.Height > 0 && this.bmi.Weight > 0) {
            this.rebmi = this.bmi.Weight / (this.bmi.Height / 100 * this.bmi.Height / 100);
            this.rebmi = this.decimalAdjust('round', this.rebmi, -1);   // 55.6
        }
        else {
            let alert = this.alertCtrl.create({
                title: 'Lỗi',
                subTitle: 'Bạn chưa nhập đủ thông tin!',
                buttons: ['OK']
            });
            alert.present();
        }
    }

    resetBMI_Click() {
        //this.navCtrl.push(SmartfoodPage);
    }
    ionViewDidLoad() {
        console.log('Hello HomePage Page');
    }

    presentPopover () {

    }

}
