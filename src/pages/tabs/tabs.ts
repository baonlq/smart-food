import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { LoginPage } from '../login/login';
import { BMICalPage } from '../bmi-cal/bmi-cal';
import { ParseService } from '../../providers/parse';

@Component({
    templateUrl: 'tabs.html',
    providers: [ParseService]
})
export class TabsPage implements OnInit {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root: any = HomePage;
    tab2Root: any = AboutPage;
    tab3Root: any = BMICalPage;

    constructor(public navCtrl: NavController,
        public parse: ParseService) {

    }

    ngOnInit() {
        console.log('ngOnInit Tabspage', this.parse.isAuthenticated());
        if (!this.parse.isAuthenticated()) {
            this.navCtrl.push(LoginPage);
        }
    }
}
