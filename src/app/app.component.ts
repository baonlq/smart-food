import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ParseService } from '../providers/parse';


@Component({
    templateUrl: 'app.html',
    providers: [ParseService]
})
export class MyApp implements OnInit {
    @ViewChild('nav') nav: NavController
    public rootPage: any = TabsPage;

    constructor(platform: Platform,
        public parse: ParseService) {

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
        });
    }

    ngOnInit() {
        console.log('ngOnInit')

    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        // helpers.debounce(this.nav.setRoot(page.component), 60, false);
        this.nav.setRoot(page);
    }
}
