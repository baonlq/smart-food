export class User {

    constructor(
        public username: string,
        public password: string,
        public passwordConfirmation: string,
        public email: string,
        public dateOfBirth: string
    ) { }

}
